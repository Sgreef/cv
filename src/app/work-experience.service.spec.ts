/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WorkExperienceService } from './work-experience.service';

describe('WorkExperienceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkExperienceService]
    });
  });

  it('should ...', inject([WorkExperienceService], (service: WorkExperienceService) => {
    expect(service).toBeTruthy();
  }));
});
