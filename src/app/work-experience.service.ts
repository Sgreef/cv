//Work experience service, to get mock data and pass it to component
import { Injectable } from '@angular/core';
import { Work } from './work';
import { WORKS } from './mock-work-experience';

@Injectable()
export class WorkExperienceService {

  //Methon that get mock data from mock-work-experience
  getWorks(): Work[] {
    return WORKS;
  }

  constructor() { }

}
