// Mock data file
import { Work } from './work';

// Work experience constant
export const WORKS: Work[] = [
  {id: 1, title: 'Software Engineer Internship', employer: 'Skype', desc: 'Sofware Engineer', active: '07/2017–09/2017'},
  {id: 2, title: 'Co-Founder', employer: 'Instalord OÜ', desc: 'IT systems development ', active: '04/2016–Present'},
  {id: 3, title: 'Co-Founder', employer: 'Kood2 OÜ', desc: 'Front-end development, Finances, Communicating with clientele '
  , active: '12/2016–Present'},
  {id: 4, title: 'Sales Consultant', employer: 'Lindomare OÜ', desc: 'Retirement funds specialist', active: '02/2016–01/2017'}
];
