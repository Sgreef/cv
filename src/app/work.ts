export class Work{
    id: number;
    title: string;
    employer: string;
    desc: string;
    active: string;
}