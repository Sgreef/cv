import {animate, Component, OnInit, state, style, transition, trigger} from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css'],
  animations: [
    trigger("elementState", [
      state("notactive", style({
        opacity: 0,
        visibility: "hidden",
      })),
      state("activate", style({
        opacity: 1,
        visibility: "visible",
      })),
      transition("* => activate", [
        style({transform: 'translateX(100%)'}),
        animate(1500)
      ]),
      transition("* => notactive", animate("1500ms ease-out")),
    ])
  ]
})
export class EducationComponent implements OnInit {
  collegeKeySubjects;
  scrollimateOptions: any = {
    mysScrollimation: {
      currentState: "notactive", // currentState is required
      states: [
        {
          method:"pxElement", // this will be true if more than 200px of the element are currently visible (scrolled)
          value: 200,
          state: "activate",
        },
        {
          method: "default",
          state: "notactive"
        }
      ]
    },
  }
  constructor() {
    this.collegeKeySubjects = [
      'Programming with C#',
      'Network Applications I: Client-Server Systems',
      'Algorithms and Data Structures (Java)',
      'Mobile Software Development for Android',
      'Introduction to Mobile Apple Technologies',
      'Database Basics',
      'User Interfaces Design and Development'
    ];
   }

  ngOnInit() {
  }

}
