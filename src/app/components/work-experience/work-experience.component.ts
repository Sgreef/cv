// Component of work experiences. Using and object (class) called Work. (Not because it is necessery)
import {animate, Component, OnInit, state, style, transition, trigger} from '@angular/core';
import { Work } from '../../work';
import { WorkExperienceService } from '../../work-experience.service';


@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css'],
  providers: [WorkExperienceService],
  animations: [
    trigger("elementState", [
      state("inactive", style({
        opacity: 0,
        visibility: "hidden",
      })),
      state("active", style({
        opacity: 1,
        visibility: "visible",
      })),
      transition("* => active", [
        style({transform: 'translateX(-100%)'}),
        animate(1500)
      ]),
      transition("* => inactive", animate("1500ms ease-out")),
    ])
  ]
})


export class WorkExperienceComponent implements OnInit {
 works: Work[];
  scrollimateOptions: any = {
    myScrollimation: {
      currentState: "inactive", // currentState is required
      states: [
        {
          method:"pxElement", // this will be true if more than 200px of the element are currently visible (scrolled)
          value: 200,
          state: "active",
        },
        {
          method: "default",
          state: "inactive"
        }
      ]
    },
  }

  constructor( private _workExperienceService: WorkExperienceService) {
      this.works = _workExperienceService.getWorks();
   }
  ngOnInit() {
  }
}
